**Prerequisites**

- Node, Python2, JDK, Git

**Install**


1. Install Android Studio

2. Configure the ANDROID_HOME environment variable

3. Clone the project using the comand "git clone https://gitlab.com/ivato/doclass/doclass-mobile.git"

4. Open the project folder on terminal

5. Install the dependencies from project using the comand "npm install"

6. Run the app using the comand "npx react-native run-android"

