import { all } from "redux-saga/effects";
import network from "./network/sagas";
import dataset from "./dataset/sagas";
import document from "./document/sagas";
import auth from "./auth/sagas";

export default function* rootReducer() {
  return yield all([auth, network, dataset, document]);
}
