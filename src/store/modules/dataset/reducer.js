import produce from 'immer';

const INITIAL_STATE = {
  datasets: [],
  dataset: {},
  loading: false,
};

export default function dataset(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@dataset/DATASETS_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@dataset/DATASETS_SUCCESS': {
        draft.datasets = action.payload.data;
        draft.loading = false;
        break;
      }

      case '@dataset/DATASETS_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@dataset/SET_DATASET': {
        draft.dataset = action.payload.dataset;
        break;
      }
    }
  });
}
