import {takeLatest, call, put, all, select} from 'redux-saga/effects';
import api from '~/config/api';
import {datasetsSuccess, datasetsFailure} from './actions';

function* datasets() {
  try {
    const response = yield call(api.get, `/dataset`);

    yield put(datasetsSuccess(response.data));
  } catch (err) {
    yield put(datasetsFailure());
    console.log(err);
  }
}

export default all([takeLatest('@dataset/DATASETS_REQUEST', datasets)]);
