import produce from "immer";
import moment from "moment";

const INITIAL_STATE = {
  email: null,
  session: null,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case "@auth/SET_EMAIL": {
        draft.email = action.payload.email;
        break;
      }

      case "@auth/NEW_SESSION": {
        const now = moment().utc().unix();
        draft.session = now;
        break;
      }
    }
  });
}
