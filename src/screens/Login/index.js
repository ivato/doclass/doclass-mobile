import React, { useState } from "react";
import { Container } from "./styles";
import Input from "~/components/Input";
import colors from "~/styles/colors";
import Button from "~/components/Button";
import { useDispatch } from "react-redux";
import { setEmail } from "~/store/modules/auth/actions";

export default function Login({ navigation }) {
  const dispatch = useDispatch();
  const [emailInput, setEmailInput] = useState("");
  return (
    <Container>
      <Input
        title="Email"
        color={colors.primary}
        value={emailInput}
        onChangeText={(text) => setEmailInput(text)}
      />

      <Button
        onPress={() => dispatch(setEmail(emailInput))}
        style={{ marginTop: 20, width: 200, height: 50 }}
        text="Save"
        background={colors.primary}
        color={colors.white}
      />
    </Container>
  );
}
