import React, { useEffect } from "react";
import { Container } from "./styles";
import DatasetRow from "~/components/DatasetRow";
import colors from "~/styles/colors";
import { useSelector, useDispatch } from "react-redux";
import { datasetsRequest, setDataset } from "~/store/modules/dataset/actions";
import Loading from "~/components/Loading";
import moment from "moment";

export default function Datasets({ navigation }) {
  const dispatch = useDispatch();
  const datasets = useSelector((state) => state.dataset.datasets);
  const loading = useSelector((state) => state.dataset.loading);

  useEffect(() => {
    dispatch(datasetsRequest());
  }, []);

  const render = () => {
    if (!loading) {
      return datasets.map((value, index) => {
        return (
          <DatasetRow
            action={() => {
              dispatch(setDataset(value));
              navigation.navigate("Documents");
            }}
            color={colors.primary}
            id={value._id}
            name={value.name}
            url={value.url}
            key={index}
          />
        );
      });
    }

    return <Loading color={colors.primary} />;
  };

  return <Container>{render()}</Container>;
}
