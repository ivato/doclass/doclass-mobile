import React, { useState, useEffect } from "react";
import {
  Container,
  DocumentHeaderButton,
  DocumentsHeader,
  Text,
  DocumentHeaderTitle,
} from "./styles";
import { useSelector, useDispatch } from "react-redux";
import { ScrollView, Dimensions } from "react-native";
import DocumentRow from "~/components/DocumentRow";
import colors from "~/styles/colors";
import DocumentViewer from "~/components/DocumentViewer";
import Loading from "~/components/Loading";
import {
  documentsRequest,
  documentRequest,
  clear,
} from "~/store/modules/document/actions";

export default function Documents({ navigation }) {
  const dispatch = useDispatch();
  const email = useSelector((state) => state.auth.email);
  const session = useSelector((state) => state.auth.session);
  const datasetName = useSelector((state) => state.dataset.dataset.name);
  const documents = useSelector((state) => state.document.documents);
  const loading = useSelector((state) => state.document.documentsLoading);
  const [openDocumentViewer, setOpenDocumentViewer] = useState(false);
  const { width, height } = Dimensions.get("screen");
  const documentsByRequest = Math.round(height / 50);

  useEffect(() => {
    dispatch(clear());
    dispatch(documentsRequest(documentsByRequest));
  }, []);

  const endList = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 600;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  return (
    <Container>
      <Text numberOfLines={1}>
        <Text bold>Email: </Text>
        {email}
      </Text>
      <Text numberOfLines={1}>
        <Text bold>Session: </Text>
        {session}
      </Text>
      <Text numberOfLines={1}>
        <Text bold>Dataset: </Text>
        {datasetName}
      </Text>

      <DocumentsHeader>
        <DocumentHeaderButton>
          <DocumentHeaderTitle>ID</DocumentHeaderTitle>
        </DocumentHeaderButton>
        <DocumentHeaderButton>
          <DocumentHeaderTitle>Certainty level</DocumentHeaderTitle>
        </DocumentHeaderButton>
      </DocumentsHeader>

      <ScrollView
        onScroll={({ nativeEvent }) => {
          if (endList(nativeEvent)) {
            dispatch(documentsRequest(documentsByRequest));
          }
        }}
        scrollEventThrottle={16}
      >
        {documents.map((value, index) => {
          return (
            <DocumentRow
              action={() => {
                setOpenDocumentViewer(true);
                dispatch(documentRequest(value._id, value.labels[0]));
              }}
              _id={value._id}
              color={
                value.labels[0] === 1 ? `${colors.success}` : `${colors.danger}`
              }
              textColor={colors.white}
            />
          );
        })}

        {loading && <Loading color={colors.primary} />}
      </ScrollView>

      <DocumentViewer
        isVisible={openDocumentViewer}
        setIsVisible={setOpenDocumentViewer}
      />
    </Container>
  );
}
