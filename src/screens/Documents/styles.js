import styled from "styled-components/native";
import colors from "~/styles/colors";

export const Container = styled.View`
  flex: 1;
  padding-top: 15px;
`;

export const Text = styled.Text`
  padding: 0 15px;
  padding-bottom: 5px;
  font-size: 18px;
  font-weight: ${(props) => (props.bold ? "bold" : "normal")};
  margin-bottom: 5px;
  color: ${colors.primary};
`;

export const DocumentsHeader = styled.View`
  padding: 0 10px;
  width: 100%;
  height: 50px;
  background-color: ${colors.primary};
  align-items: center;
  flex-direction: row;
`;

export const DocumentHeaderButton = styled.TouchableOpacity`
  width: 50%;
  height: 100%;
  justify-content: center;
`;

export const DocumentHeaderTitle = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: ${colors.white};
`;
