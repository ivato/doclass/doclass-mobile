export default {
  primary: '#67A1FF',
  success: '#12BA32',
  danger: '#E51062',
  warning: '#F5BD00',

  inactiveBlack: '#00000044',
  inactiveWhite: '#FFFFFF44',

  //Neutrals colors
  white: '#FFFFFF',
  black: '#000000',

  transparent: '#00000000',
};
