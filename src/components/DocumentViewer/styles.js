import styled from 'styled-components/native';
import Modal from 'react-native-modal';
import colors from '~/styles/colors';

export const ModalContainer = styled(Modal)`
  padding: 0;
  margin: 0;
`;

export const Container = styled.View`
  width: 100%;
  background-color: ${colors.white};
  position: absolute;
  bottom: 0;
  height: 70%;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  padding-top: 15px;
  justify-content: center;
  align-items: center;
`;

export const TextBox = styled.Text`
  text-align: justify;
  font-size: 16px;
  padding: 0 15px;
`;

export const Buttons = styled.View`
  width: 100%;
  height: 60px;
  padding: 0 15px;
  align-items: center;
  flex-direction: row;
`;
