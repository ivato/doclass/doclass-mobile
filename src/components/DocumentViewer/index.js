import React from 'react';
import {ModalContainer, Container, TextBox, Buttons} from './styles';
import {ScrollView} from 'react-native';
import Button from '../Button';
import colors from '~/styles/colors';
import {useSelector} from 'react-redux';
import Loading from '../Loading';

export default function DocumentViewer({isVisible, setIsVisible}) {
  const document = useSelector((state) => state.document.document);
  const loading = useSelector((state) => state.document.documentLoading);

  const render = () => {
    if (loading) {
      return <Loading color={colors.primary} />;
    }

    return (
      <>
        <ScrollView>
          <TextBox>{document.content}</TextBox>
        </ScrollView>
        <Buttons>
          <Button
            style={{marginRight: 5}}
            text="YES"
            background={colors.success}
            onPress={() => console.log('YES')}
          />
          <Button
            onPress={() => console.log('NO')}
            text="NO"
            background={colors.danger}
          />
        </Buttons>
      </>
    );
  };

  return (
    <ModalContainer
      isVisible={isVisible}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      onRequestClose={() => setIsVisible(false)}
      animationInTiming={300}
      onBackdropPress={() => setIsVisible(false)}
      supportedOrientations={['portrait']}>
      <Container>{render()}</Container>
    </ModalContainer>
  );
}
