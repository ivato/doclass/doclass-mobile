import React from 'react';
import {ActivityIndicator} from './styles';
import colors from '~/styles/colors';

export default function Loading({color = colors.primary, size = 'small'}) {
  return <ActivityIndicator color={color} size={size} />;
}
