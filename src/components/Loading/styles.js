import styled from "styled-components/native";

export const ActivityIndicator = styled.ActivityIndicator.attrs((props) => {
  return {
    size: props.size,
    color: props.color,
  };
})`
  margin: 20px 0;
`;
