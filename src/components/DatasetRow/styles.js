import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  width: 100%;
  margin-top: 15px;
  height: 80px;
  border: 1px solid ${(props) => props.color};
  border-radius: 5px;
  justify-content: center;
  padding: 0 10px;
`;

export const Text = styled.Text`
  font-size: 16px;
  color: ${(props) => props.color};
  font-weight: ${(props) => (props.bold ? 'bold' : 'normal')};
`;
