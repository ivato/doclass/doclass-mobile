import React from 'react';
import {Container, Text} from './styles';

export default function DatasetRow({id, name, url, color, action}) {
  return (
    <Container onPress={action} color={color}>
      <Text color={color} numberOfLines={1}>
        <Text color={color} bold={true}>
          Name:{' '}
        </Text>
        {name}
      </Text>

      <Text color={color} numberOfLines={1}>
        <Text color={color} bold={true}>
          URL:{' '}
        </Text>
        {url}
      </Text>
    </Container>
  );
}
