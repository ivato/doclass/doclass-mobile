import React from "react";
import { Container, Text } from "./styles";

export default function DocumentRow({ _id, color, textColor, action }) {
  return (
    <Container onPress={action} color={color}>
      <Text textColor={textColor}>{_id}</Text>
      <Text textColor={textColor}>87%</Text>
    </Container>
  );
}
