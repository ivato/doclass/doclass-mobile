import styled from "styled-components/native";
import colors from "~/styles/colors";

export const Container = styled.TouchableOpacity`
  width: 100%;
  height: 50px;
  border-top-width: 1px;
  border-top-color: ${colors.black};
  background-color: ${(props) => props.color};
  align-items: center;
  padding: 0 10px;
  flex-direction: row;
`;

export const Text = styled.Text`
  font-size: 16px;
  color: ${(props) => props.textColor};
  font-weight: bold;
  width: 50%;
`;
