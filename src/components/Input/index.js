import React from "react";
import { Container, Title, TextInput } from "./styles";
import colors from "~/styles/colors";

export default function Input({ color = colors.primary, title, ...rest }) {
  return (
    <Container>
      {title && <Title colorI={color}>{title}</Title>}
      <TextInput colorI={color} {...rest}></TextInput>
    </Container>
  );
}
