import "react-native-gesture-handler";
import React, { useEffect } from "react";
import Routes from "../routes";
import { useDispatch } from "react-redux";
import { newSession } from "./store/modules/auth/actions";

export default function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(newSession());
  }, []);

  return <Routes />;
}
