/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Sudden from './src/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Sudden);
